let requestJson = require('request-json');
let url = require('../../app')


module.exports = {
  userValidate,
  auth,
  signup,
  logout
}

function generateClientMlab(params) {
  if (params) {
    return requestJson.createClient(url.urlUsers + params);
  }
  else {
    return requestJson.createClient(url.urlUsers);
  }
}

//Function to store data from user loged succesfull
function storeUserLoged(data, res,next) {
  let token= data
  
  //console.log('topken'+ JSON.stringify(token));
  let respBody = {
    'uid': data.uid,
    'email': data.email,
    'displayName': data.displayName,
    'photoURL': data.photoURL,
    'session': "active",
    'token': data.qa,
    'sessionDate': Date()
  };
  //Function to Make URL
  let UrlMlab = generateClientMlab();

  //Store Data in MLAB
  UrlMlab.post('', respBody, function (error, resMlab, body) {
    if (error) {
      console.log('Se ha presentado un error de tipo:' + error);
    }
    else {
      console.log('\nAlmacen en Mlab');
      console.log('Fecha '+ Date());
      res.send(body);
      //next();
    }
  })

}

//This function is called when is execute the method POST with URL "/signup"

function signup(req, res) {
  //Use Init user admin firebase
  url.admin.auth().createUser({
    email: "nes1@example.com",
    emailVerified: false,
    phoneNumber: "+11122334451",
    password: " ",
    displayName: "Nestor2 Sanchez",
    photoURL: "http://www.example.com/12345628/photo.png",
    disabled: false
  })
    .then(function (userRecord) {
      // See the UserRecord reference doc for the contents of userRecord.
      console.log("Successfully created new user:", userRecord.uid);
      return res.send(userRecord);
    })
    .catch(function (error) {
      console.log("Error creating new user:", error);
      return res.send(error);
    });
}

//Steps to User authentication
const stepsAuth = async (email, password, res) => {
  var user = url.firebaseAuth.auth().currentUser;
  //url.firebaseAuth.auth().onAuthStateChanged(function (user) {
    if (user) {
      console.log('usuario autenticado en firebase'+ user);
    } else {
      console.log('Usuario no autenticado aun ' + user);
      //Use init Authentication firebase
      url.firebaseAuth.auth().signInWithEmailAndPassword(email, password)
        .then(responseAuth => {
          return storeUserLoged(responseAuth.user, res);

        })
        .catch(function (error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          console.log('err token ' + JSON.stringify(errorMessage));
          //res.redirect('../error');
          res.status(403).send(error);
        });
    }
  //});
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++
//authentication from User
/*
Body from postman
{
"user":"nes@example.com",
"password": "secretPassword"
}
*/
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++

//Validate user in Mlab
function userValidate(req, res, next) {
  let email = req.body.user;
  let userFilte = JSON.stringify({ "email": email })
  let param = '&q=' + userFilte + '&c=true'
  let urlUser = generateClientMlab(param);
  urlUser.get('', function (err, resM, body) {
    if (err) {
      return err;
    }
    else {
      if (body) {
        console.log('Usuario Ya logueado'+ body);
        res.send('Usuario ' + email + ' Ya logueado ');

      }
      else {
        console.log('auth with Firebase');
        next();
      }
    }
  });
}


//This function is called in request user with method POST and URL Login
function auth(req, res,next) {
  
  let email = req.body.user,
    password = req.body.password;
  stepsAuth(email, password, res);
  
};

function cleanSessionMlab(req,res){
  let email = req.body.user;
  let disableSession= { "user": email, "Sesion": "disabled" }
  let userFilte = JSON.stringify({ "email": email});
  let param = '&q=' + userFilte;
  let urlUser = generateClientMlab(param);

  urlUser.put('',disableSession, function (err, resM, body) {
    if (err) {
      console.log('error put');
      return err;
    }
    else {
      if (body) {
        console.log('Cierre de session '+body);
        res.send('Usuario ' + email + '  deslogueado ');

      }
      else {
       
        next();
      }
    }
  });
}

function logout(req, res) { 
  let sessionUser=req.params.email;
  var user = url.firebaseAuth.auth().currentUser;
  console.log('user  logout '+ user);
  if(user)
    {
      url.firebaseAuth.auth().signOut()
        .then(function () {
        cleanSessionMlab(req, res);  
        //res.send('Fin de sesion')
        }).catch(function (error) {
          console.log('error de fin se sesion')
        });

    }

}
