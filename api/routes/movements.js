//Peticion a Mlab
var requestJson = require('request-json');
var express = require('express');
var apiMovements = express.Router();
let apiSec= express.Router();
let movementsControl= require('../controllers/movementsController');
let userControl= require('../controllers/userController');


apiSec.get('/movements', [movementsControl.validateToken, movementsControl.getAllMovements]);
//apiMovements.get('/movements', movementsControl.getAllMovements);


// var url= require('../../app');

// var clienteMlab = requestJson.createClient(url.urlMovements);
// api.get('/movements', function (req, res) {
//   clienteMlab.get('', function (err, resM, body) {
//     if(err){
//       console.log('Se ha presentado un error de tipo:'+ err);
//     }
//     else{
//       res.send(body);
//     }
//   });
// });

// // API POST to MLAb
// api.post('/movements', function(req, res){
//   clienteMlab.post('', req.body, function(err, resM, body){
//     if(err){
//       console.log('Se ha presentado un error de tipo:'+ err);
//     }
//     else{
//       res.send(body);
//     }
//   });
// });

module.exports.apiSec = apiSec;
module.exports.apiMovements = apiMovements;