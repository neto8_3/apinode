let express = require('express');
let bodyParse= require('body-parser');
let app = express();
let adminFirebase = require('firebase-admin');
let firebaseAuth = require('firebase');

let serviceAccount = require("./config/api-auth-nes-firebase-adminsdk-lbunr-974eb320ea.json");

module.exports.admin= adminFirebase.initializeApp({
  credential: adminFirebase.credential.cert(serviceAccount),
  databaseURL: "https://api-auth-nes.firebaseio.com"
});


//Config Firebase to authenticated
let config = {
  apiKey: "AIzaSyDwyAwXIuWQmfRd7K3bbkitzFWVJSyMUo0",
  authDomain: "api-auth-nes.firebaseapp.com",
  databaseURL: "https://api-auth-nes.firebaseio.com",
  projectId: "api-auth-nes",
  storageBucket: "api-auth-nes.appspot.com",
  messagingSenderId: "226425325627"
};

module.exports.firebaseAuth=firebaseAuth.initializeApp(config);




// Configuration headers to CORS
app.use(bodyParse.json());
app.use(function(req, res, next){
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Headers', "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });



// Create URL to Codelab Movements


// Configuracion de app Mlab
  var prop= require('./config/properties');

  module.exports.urlMovements = prop.host + prop.endpoint+ '/movements?apiKey=' + prop.apikey;
  module.exports.urlAccounts = prop.host + prop.endpoint+ '/accounts?apiKey=' + prop.apikey;
  module.exports.urlTransactions = prop.host + prop.endpoint+ '/transactions?apiKey=' + prop.apikey;
  module.exports.urlUsers= prop.host + prop.endpoint+ '/users?apiKey=' + prop.apikey;

// Use Api Movements
  let user_routes = require('./api/routes/user');
  app.use('/api/user', [user_routes.api, user_routes.api1]);

  let movements_routes= require ('./api/routes/movements');
  app.use('/api', [movements_routes.apiSec,movements_routes.apiMovements]);

  module.exports = app;
